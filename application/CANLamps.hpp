#ifndef __APPLICATION_LAMPS_HPP__
#define __APPLICATION_LAMPS_HPP__


#include "CAN.hpp"

#include <message/Receiver.hpp>
#include <message/Message.hpp>

#include "MainQueue.hpp"
namespace application
{
namespace CANLamps
{
struct can_lamps_ramp_message_t
{
    uint32_t number : 8;
    uint32_t newState : 16;
    uint32_t reserved0 : 8;
    int32_t incState : 16;
    int32_t reserved1 : 16;
};

class Receiver : public message::Receiver<application::CAN::data_t>, 
    public message::Receiver<application::CANLamps::can_lamps_ramp_message_t>
{
public:
    Receiver(message::ReceiverQueue& q) : message::Receiver<application::CAN::data_t>(q),
        message::Receiver<application::CANLamps::can_lamps_ramp_message_t>(q) {}
    virtual void receive(message::Message<application::CAN::data_t>&) final override;
    virtual void receive(message::Message<application::CANLamps::can_lamps_ramp_message_t>&) final override;
};

}
}
#endif //__APPLICATION_LAMPS_HPP__
