//std
#include <algorithm> //std::move

//lib


//local
#include <lowLevel/CAN.hpp>
#include "CAN.hpp" //application/CAN.hpp
#include "CANLamps.hpp"
#include "MainQueue.hpp"

/*
 * 
 */

application::CANLamps::Receiver canLamps(MainQueue::queue);

/*
 * exampleid
 * 00020400
 * 0000 0000 0000 0010 0000 0100 0000 0000
 * uuuu uuuu ummm mmmt tttt ttll llll lrrr
 * 0000 0000 0001 0000 0011 1000 0000 0001
 * 
 * 
 * format for can send
 * 0uuu uuuu uumm 00mm mmtt tttt tlll llll
 * 0x00004080
 * cansend can0 00004080#0000000000000000
 * cansend can0 00004080#0001000000000000
 */

// structs are defined LSB->MSB (top to bottom)

struct can_id_decode_t {
    union {
        uint32_t id;
        /*struct {
            uint32_t reserved : 3; // first 3 LSB bits are reserved
            uint32_t extId : 18;
            uint32_t stdId : 11;
        };*/
        /*
         * TODO: To be discussed. Marc thinks that this might be a good
         * way to encode CAN messages slit into 
         * - locations (rooms, areas, ...) use 0x7F for broadcast
         * - have a type id (switch, lamp, indoor sensor, air quality sensor)
         *   outdoor sensors, etc
         * - a message id per type (so 32 messages per type)
         * - a unique address per device in this way also multicast groups
         *   can be implemented (i.e. using adress 0x1FFFF als broadcast)
         * - locationId and uniqueId can be filtered
         *   typeId can be partly filtered but also dispached here
         * - messageId is fully dispatched here
         * 
         */
        struct {
            uint32_t reserved : 3; // first 3 LSB bits are reserved
            uint32_t messageId : 4;
            uint32_t extId : 14;
            uint32_t stdId : 11;
        };
    };
};


void application::CAN::send(uint32_t messageId, lowLevel::CAN::data_t data)
{
    can_id_decode_t decodedId;
    decodedId.id = (lowLevel::CAN::getOwnID() + 4000000) << 3;
    decodedId.messageId = messageId;
    lowLevel::CAN::send(decodedId.id, data);
}

/*
 * This call comes from in interrupt. Sync with main thread using a message.
 * In this way also dispatch the can messages to thier destination
 */
void lowLevel::CAN::receive(uint32_t id, lowLevel::CAN::data_t data)
{
    can_id_decode_t decodedId;
    decodedId.id = id;
    
    application::CAN::data_t aData { static_cast<uint8_t>(decodedId.messageId), data };
    
    
    //if (decodedId.typeId == 1)
    //{
        /* dispatch the raw data from the can message to the receiver.
         * let the receiver decode the message
         */
        message::Message<application::CAN::data_t>::send(InterruptQueue::queue, canLamps, message::Event::CAN_DATA, std::move(aData));
    //}
    
    //decodedId.typeId = 0xE;
    //decodedId.messageId = 0xE;
}
