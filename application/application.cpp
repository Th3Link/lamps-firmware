#include "lowLevel/applicationHook.h"
#include "lowLevel/LED.hpp"
#include "lowLevel/CAN.hpp"
#include "lowLevel/Lamps.hpp"
#include "lowLevel/LowLevel.hpp"

#include "MainQueue.hpp"

#include <chrono>
#include <message/Message.hpp>
#include <message/Queue.hpp>

class Dimmer : public message::Receiver<lowLevel::LED::Instance>
{
public:
    Dimmer(message::ReceiverQueue& q) : message::Receiver<lowLevel::LED::Instance>(q)
    {
        
    }
    virtual void receive(message::Message<lowLevel::LED::Instance>& m) final override
    {
        lowLevel::LED::value(m.data, 1-lowLevel::LED::value(m.data));
        message::Message<lowLevel::LED::Instance>::send(queue, *this, message::Event::UPDATE, 
            std::move(m.data), std::chrono::milliseconds(1000));
    }
};

class KeepAlive : public message::Receiver<int>
{
public:
    KeepAlive(message::ReceiverQueue& q) : message::Receiver<int>(q)
    {
        
    }
    virtual void receive(message::Message<int>& m) final override
    {
        lowLevel::LowLevel::keepAlive();
        message::Message<int>::send(queue, *this, message::Event::UPDATE, 
            std::move(m.data), std::chrono::milliseconds(500));
    }
};

// dont put large things on the stack
MainQueue::queue_t MainQueue::queue;
InterruptQueue::queue_t InterruptQueue::queue;

void startApplication()
{
    lowLevel::LowLevel::init();
    lowLevel::LED::init(lowLevel::LED::Instance::L1);
    lowLevel::LED::init(lowLevel::LED::Instance::L2);
    lowLevel::CAN::init();
    lowLevel::Lamps::init();
    
    Dimmer dimmer(MainQueue::queue);
    message::Message<lowLevel::LED::Instance>::send(
        MainQueue::queue, dimmer, message::Event::UPDATE, 
        lowLevel::LED::Instance::L1, std::chrono::milliseconds(1000));
    
    message::Message<lowLevel::LED::Instance>::send(
        MainQueue::queue, dimmer, message::Event::UPDATE, 
        lowLevel::LED::Instance::L2, std::chrono::milliseconds(1333));
    
    
    KeepAlive keepAlive(MainQueue::queue);
    message::Message<int>::send(MainQueue::queue, keepAlive, message::Event::UPDATE, 
        0, std::chrono::milliseconds(500));

    while(true)
    {
        MainQueue::queue.dispatch();
        InterruptQueue::queue.dispatch();
    }
    
}
