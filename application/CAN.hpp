#ifndef __APPLICATION_CAN_HPP__
#define __APPLICATION_CAN_HPP__

//std
#include <etl/vector.h>
#include <cstdint>

//lib

//local
#include <lowLevel/CAN.hpp>

namespace application
{
    namespace CAN
    {
        namespace ID
        {
            constexpr uint32_t TemperatureSensors = 14;
        } 
        struct data_t
        {
            uint8_t messageId;
            lowLevel::CAN::data_t data;
        };
        void send(uint32_t messageId, lowLevel::CAN::data_t data);
    }
}

#endif //__APPLICATION_CAN_HPP__
