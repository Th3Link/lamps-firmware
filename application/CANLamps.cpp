//std
#include <algorithm>
#include <chrono>

//lib

//local
#include "CANLamps.hpp"
#include <lowLevel/Lamps.hpp>

/*
 * How do we encode switch on and off and a time?
 * 8 bytes
 */

constexpr unsigned int DEFAULT_RAMPTIME_MS = 400;
constexpr unsigned int RAMPTIME_SLICE = 10;
#pragma pack(push,1)
struct can_lamps_message_t
{
    union
    {
        uint8_t data[8];
        struct
        {
            uint32_t number : 8;
            uint32_t state : 16;
            uint32_t ramptime_ms : 16;
            uint32_t reserved1 : 24;
        };
    };
};
#pragma pack(pop)
void application::CANLamps::Receiver::receive(message::Message<application::CAN::data_t>& m)
{
    can_lamps_message_t clm;
    clm.ramptime_ms = DEFAULT_RAMPTIME_MS;
    // m is a message holding the generic data in this case application::CAN::data_t
    // application::CAN::data_t has also a field "data" which is of type lowLevel::CAN::data_t
    std::copy(m.data.data.begin(), m.data.data.end(), clm.data);
    
    auto currentState = lowLevel::Lamps::get(clm.number);
    auto diffState = static_cast<int>(clm.state) - static_cast<int>(currentState);
    int incState = (diffState * static_cast<int>(RAMPTIME_SLICE)) / static_cast<int>(clm.ramptime_ms);
    
    application::CANLamps::can_lamps_ramp_message_t clrm;
    clrm.number = clm.number;
    clrm.newState = clm.state;
    clrm.incState = incState;
    
    message::Message<application::CANLamps::can_lamps_ramp_message_t>::send(
        MainQueue::queue, *this, message::Event::UPDATE, std::move(clrm), 
        std::chrono::milliseconds(RAMPTIME_SLICE));
    
}

void application::CANLamps::Receiver::receive(message::Message<application::CANLamps::can_lamps_ramp_message_t>& m)
{
    auto currentState = lowLevel::Lamps::get(m.data.number);
    auto nextstate = currentState + m.data.incState;
    
    if ((m.data.incState == 0) ||
        ((m.data.incState > 0) && ((m.data.incState + currentState) > m.data.newState)) || 
        ((m.data.incState < 0) && ((m.data.incState + currentState < m.data.newState))))
    {
        // set final brightness
        lowLevel::Lamps::set(m.data.number, m.data.newState);
        return;
    }
    
    lowLevel::Lamps::set(m.data.number, nextstate);
    
    message::Message<application::CANLamps::can_lamps_ramp_message_t>::send(
        MainQueue::queue, *this, message::Event::UPDATE, std::move(m.data), 
        std::chrono::milliseconds(RAMPTIME_SLICE));
}
