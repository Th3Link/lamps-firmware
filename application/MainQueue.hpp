#ifndef __MAINQUEUE_HPP__
#define __MAINQUEUE_HPP__

#include <message/Queue.hpp>

/*
 * This header is just to make the queue available to other units
 * than application.cpp
 */

namespace MainQueue
{
    // typedefs reduces programmers mistakes of messung up the numbers
    using queue_t = message::Queue<256, 256>;
    // queue is declared here and defined somewhere else 
    // (the linker will put it together)
    extern queue_t queue;
}

namespace InterruptQueue
{
    // typedefs reduces programmers mistakes of messung up the numbers
    using queue_t = message::Queue<64, 128>; // two can message and 4 timeout fit in there
    // queue is declared here and defined somewhere else 
    // (the linker will put it together)
    extern queue_t queue;
}

#endif //__MAINQUEUE_HPP__
