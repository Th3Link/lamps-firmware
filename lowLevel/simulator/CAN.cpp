//std

//lib

//local
#include <lowLevel/CAN.hpp>
#define ownID 0xA000//(*(uint32_t *)0x8003000)

//suppress name mengling for callback functions
using namespace lowLevel;

// structs are defined LSB->MSB (top to bottom)
struct can_id_t {
    union {
        uint32_t id;
        struct {
            uint32_t reserved : 3;
            uint32_t extId : 18;
            uint32_t stdId : 11;
        };
    };
};

void lowLevel::CAN::init()
{
    uint32_t id = 0;

    CAN::data_t data;
    CAN::send(id, data);
    
}

void lowLevel::CAN::send(uint32_t id, CAN::data_t data)
{

}

uint32_t lowLevel::CAN::getOwnID()
{
    return ownID;
}
