#include "lowLevel/LED.hpp"
#include <iostream>
#include <array>

static std::array<unsigned int, 
    static_cast<unsigned int>(lowLevel::LED::Instance::count)> leds
{
    0,
    0
};

void lowLevel::LED::init(lowLevel::LED::Instance instance)
{
    lowLevel::LED::value(instance, 0);
    std::cout << "LED init" << std::endl;
}

void lowLevel::LED::value(lowLevel::LED::Instance instance, unsigned int state)
{
    std::cout << "LED set to " << state << std::endl;
    auto i = static_cast<unsigned int>(instance);
    leds[i] = state;
}

unsigned int lowLevel::LED::value(lowLevel::LED::Instance instance)
{
    auto i = static_cast<unsigned int>(instance);
    return leds[i];
}
