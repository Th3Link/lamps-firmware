#ifndef __LOWLEVEL_HPP__
#define __LOWLEVEL_HPP__

//std
#include <cstdint>

//lib

//local

namespace lowLevel
{
    namespace LowLevel
    {
        void init();
        void keepAlive();
        void sleep();
    }
}

#endif //__LOWLEVEL_HPP__
