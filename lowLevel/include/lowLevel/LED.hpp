#ifndef __LOWLEVEL_LED_HPP__
#define __LOWLEVEL_LED_HPP__

namespace lowLevel
{
namespace LED
{
    enum class Instance
    {
        L1, L2, count
    };
    void init(Instance);
    void value(Instance, unsigned int);
    unsigned int value(Instance);
}
}
#endif //__LOWLEVEL_LED_HPP__
