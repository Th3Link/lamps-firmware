#ifndef __LOWLEVEL_LAMPS_HPP__
#define __LOWLEVEL_LAMPS_HPP__

//std
#include <cstdint>

//lib

//local

namespace lowLevel
{
namespace Lamps
{
    constexpr unsigned int count = 14;
    using number_t = uint8_t;
    using state_t = uint16_t;
    void init();
    void set(number_t, state_t);
    state_t get(number_t);
}
}
#endif //__LOWLEVEL_LAMPS_HPP__
