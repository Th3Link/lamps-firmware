#ifndef __CAN_HPP__
#define __CAN_HPP__

//std
#include <etl/vector.h>
#include <cstdint>
//lib

//local

namespace lowLevel
{
    namespace CAN
    {
        constexpr unsigned int BYTES_PER_MESSAGE = 8;
        // for shorter writing
        using data_t = etl::vector<uint8_t, BYTES_PER_MESSAGE>;
        void init();
        void send(uint32_t id, data_t data);
        void receive(uint32_t id, data_t data);
        uint32_t getOwnID();
        // we should declare canids here
        
    }
}

#endif //__CAN_HPP__
