//std

//lib

//local
#include <lowLevel/LowLevel.hpp>
#include "stm32f1xx_hal.h"

using namespace lowLevel;

extern IWDG_HandleTypeDef hiwdg;

void LowLevel::init()
{

}

void LowLevel::keepAlive()
{
    // The STM32 Independent Watchdog has its own dedicated clock running at 40kHz
    // (1ms per tick). Prescaler should be configured in cubeMx with 16 and the reload
    // value set to 0xFFF (4095). This results in a timeout of 1.6ms (see reference manual)
    // so we plan to feed every 500ms is plenty enough. Feeding is done over the message
    // queue and queues in iterative. Misses in queueing leads to a restart.
    HAL_IWDG_Refresh(&hiwdg);
}

void LowLevel::sleep()
{
    
}
