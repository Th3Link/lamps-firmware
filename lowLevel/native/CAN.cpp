//std
#include <algorithm>

//lib

//local
#include <lowLevel/CAN.hpp>
#include <stm32f1xx_hal.h>
#define ownID 0xA000//(*(uint32_t *)0x8003000)

//suppress name mengling for callback functions
extern "C"
{
    void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef* hcan);
    void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan);
}

extern CAN_HandleTypeDef hcan;

using namespace lowLevel;

// structs are defined LSB->MSB (top to bottom)
struct can_id_t {
    union {
        uint32_t id;
        struct {
            uint32_t reserved : 3;
            uint32_t extId : 18;
            uint32_t stdId : 11;
        };
    };
};

void lowLevel::CAN::init()
{
    uint32_t id = 0;

    //Broadcast
    id =  ownID+ 4000000;
    id <<=3;
    CAN_FilterTypeDef myFilter;
    myFilter.FilterBank                   = 0;
	myFilter.FilterMode             = CAN_FILTERMODE_IDMASK;
	myFilter.FilterScale            = CAN_FILTERSCALE_32BIT;
	myFilter.FilterIdHigh           = id >> 16;
	myFilter.FilterIdLow            = (id & 0x0000FFFF)  | 0x04;
	myFilter.FilterFIFOAssignment   = 0;
	myFilter.FilterActivation       = ENABLE;
	uint32_t Mask = 0xFFFFFFF0 << 3;
	myFilter.FilterMaskIdHigh = Mask >> 16;
	myFilter.FilterMaskIdLow = (Mask & 0x0000FFFF) | 0x04;
	HAL_CAN_ConfigFilter(&hcan,&myFilter);
    
    // Message Pending Interrupt for FIFO0
    HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING);
    
    HAL_CAN_Start(&hcan);
    
    CAN::data_t data;
    CAN::send(id, data);
    
}

void lowLevel::CAN::send(uint32_t id, CAN::data_t data)
{
    uint8_t datatx[8];
    
    CAN_TxHeaderTypeDef header;
    
    can_id_t canId;
    canId.id = id;
    header.DLC = data.size();
	header.ExtId = canId.extId;
	header.StdId = canId.stdId;
    std::copy(data.begin(), data.end(), datatx);
	header.IDE = CAN_ID_EXT;
	header.RTR = CAN_RTR_DATA;
    
    
    uint32_t msgBox;
    // send message using a timeout (in ticks)

    HAL_CAN_AddTxMessage(&hcan, &header, datatx, &msgBox);
    // TODO: handle errors
}

uint32_t lowLevel::CAN::getOwnID()
{
    return ownID;
}

/**
 * This is a forwarded interrupt origining from 
 * 1. HAL_CAN_IRQHandler
 * 2. CAN_Receive_IT
 * 3. HAL_CAN_RxCpltCallback (this)
 * @see stm32f1xx_hal_can.c
 */
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef* hcan)
{

    // when release fifo0 ISR Bit
	uint8_t datarx[8];
    CAN_RxHeaderTypeDef header;
    HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &header, datarx);
    
    can_id_t canId;
    canId.reserved = 0;
    canId.stdId = header.StdId;
    canId.extId = header.ExtId;
    
    // use the constructor of elt::vector to copy the array
    CAN::data_t data(std::begin(datarx), std::end(datarx));
    
    // pass the CAN::data_t by move ref up to the queue. no data needs to be copied
    CAN::receive(canId.id, std::move(data));

    HAL_CAN_ActivateNotification(hcan, CAN_IT_RX_FIFO0_MSG_PENDING);

}
