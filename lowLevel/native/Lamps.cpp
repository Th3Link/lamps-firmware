//std
#include <array>

//lib

//local
#include <lowLevel/Lamps.hpp>
#include <stm32f1xx_hal.h>
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;

struct lamps_pwm_t
{
    lowLevel::Lamps::number_t number;
    TIM_HandleTypeDef* handle;
    uint32_t channel;
};

std::array<lowLevel::Lamps::state_t, lowLevel::Lamps::count> lampstates;

const std::array<lamps_pwm_t, lowLevel::Lamps::count> lamps_pwm_lookup {{
    {0, &htim2, TIM_CHANNEL_1},
    {1, &htim2, TIM_CHANNEL_2},
    {2, &htim2, TIM_CHANNEL_3},
    {3, &htim2, TIM_CHANNEL_4},
    {4, &htim3, TIM_CHANNEL_1},
    {5, &htim3, TIM_CHANNEL_2},
    {6, &htim3, TIM_CHANNEL_3},
    {7, &htim3, TIM_CHANNEL_4},
    {8, &htim4, TIM_CHANNEL_2},
    {9, &htim4, TIM_CHANNEL_1},
    {10, &htim1, TIM_CHANNEL_1},
    {11, &htim1, TIM_CHANNEL_2},
    {12, &htim4, TIM_CHANNEL_3},
    {13, &htim4, TIM_CHANNEL_4}
}};
    
void lowLevel::Lamps::init()
{
    HAL_TIM_Base_Start(&htim1);
    HAL_TIM_Base_Start(&htim2);
    HAL_TIM_Base_Start(&htim3);
    HAL_TIM_Base_Start(&htim4);
    
    for (auto& lamps_pwm : lamps_pwm_lookup)
    {
        set(lamps_pwm.number, 0);
    }
}

void lowLevel::Lamps::set(lowLevel::Lamps::number_t number, lowLevel::Lamps::state_t state)
{
    if (number > lowLevel::Lamps::count)
    {
        //TODO: log error
        return;
    }
    
    lampstates[number] = state;
    
    TIM_OC_InitTypeDef sConfigOC = {0};
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = state;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCNPolarity = TIM_OCNPOLARITY_LOW;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    sConfigOC.OCIdleState = TIM_OCIDLESTATE_SET;
    sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_SET;
    HAL_TIM_PWM_Stop(lamps_pwm_lookup[number].handle, 
        lamps_pwm_lookup[number].channel);
    HAL_TIM_PWM_ConfigChannel(lamps_pwm_lookup[number].handle, 
        &sConfigOC, lamps_pwm_lookup[number].channel);
    HAL_TIM_PWM_Start(lamps_pwm_lookup[number].handle, 
        lamps_pwm_lookup[number].channel);
}

lowLevel::Lamps::state_t lowLevel::Lamps::get(lowLevel::Lamps::number_t number)
{
    if (number > lowLevel::Lamps::count)
    {
        //TODO: log error
        return 0;
    }
    return lampstates[number];
}
