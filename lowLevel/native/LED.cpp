#include "lowLevel/LED.hpp"
#include "stm32f1xx_hal.h"
#include "main.h"

#include <array>
#include <tuple>

static std::array<std::tuple<decltype(LED0_GPIO_Port), decltype(LED0_Pin), unsigned int>, 
    static_cast<unsigned int>(lowLevel::LED::Instance::count)> leds
{
    std::make_tuple(LED0_GPIO_Port, LED0_Pin, 0),
    std::make_tuple(LED1_GPIO_Port, LED1_Pin, 0),
};

void lowLevel::LED::init(lowLevel::LED::Instance instance)
{
    lowLevel::LED::value(instance, 0);
}

void lowLevel::LED::value(lowLevel::LED::Instance instance, unsigned int state)
{
    auto i = static_cast<unsigned int>(instance);
    std::get<2>(leds[i]) = state;
    HAL_GPIO_WritePin(std::get<0>(leds[i]), std::get<1>(leds[i]), 
        (state ? GPIO_PIN_SET : GPIO_PIN_RESET));
}

unsigned int lowLevel::LED::value(lowLevel::LED::Instance instance)
{
    auto i = static_cast<unsigned int>(instance);
    return std::get<2>(leds[i]);
}
